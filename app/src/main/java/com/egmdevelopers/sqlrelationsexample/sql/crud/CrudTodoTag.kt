package com.egmdevelopers.sqlrelationsexample.sql.crud

import android.content.ContentValues
import com.egmdevelopers.sqlrelationsexample.sql.DatabaseHelper
import com.egmdevelopers.sqlrelationsexample.sql.constants.*
import java.util.*

/****************************************************************************************************/
/**          CREATE                                                                                 */
/****************************************************************************************************/
fun DatabaseHelper.createTodoTag(todoId: Long, tagId: Long): Long {
    val db = this.writableDatabase

    val values = ContentValues()
    values.apply {
        put(COL_TODO_ID, todoId)
        put(COL_TAG_ID, tagId)
        put(COL_CREATED_AT, Date().time)
    }

    // Insertamos el registro y retornamos el id generado por la db
    return db.insert(TODO_TAG_TABLE_NAME, null, values)
}