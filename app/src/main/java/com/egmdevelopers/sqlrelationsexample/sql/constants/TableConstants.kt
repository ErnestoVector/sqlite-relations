package com.egmdevelopers.sqlrelationsexample.sql.constants

/*
 * Version 1 = "tags"
 * | NAME       | TYPE      | CONSTRAINTS   |
 * |------------|-----------|---------------|
 * | id         | INTEGER   | PK            |
 * | tag_name   | TEXT      | --            |
 * | created_at | DATETIME  | --            |
 */

/*
 * Version 1 = "todos"
 * | NAME       | TYPE      | CONSTRAINTS   |
 * |------------|-----------|---------------|
 * | id         | INTEGER   | PK            |
 * | note       | TEXT      | --            |
 * | created_at | DATETIME  | --            |
 */

/*
 * Version 1 = "todo_tags"
 * | NAME       | TYPE      | CONSTRAINTS   |
 * |------------|-----------|---------------|
 * | id         | INTEGER   | PK            |
 * | todo_id    | INTEGER   | FK            |
 * | tag_id     | INTEGER   | FK            |
 */

// Nombre de la base de datos
const val DB_NAME       = "todoManager"
const val DB_VERSION    = 1

// Nombre de las tablas que la componen
const val TODO_TABLE_NAME       = "todos"
const val TAG_TABLE_NAME        = "tags"
const val TODO_TAG_TABLE_NAME   = "todo_tags"

// Nombre de columnas para crear las tablas
const val COL_ID                = "id"
const val COL_CREATED_AT        = "created_at"
const val COL_TODO              = "todo"
const val COL_STATUS            = "status"
const val COL_TAG_NAME          = "tag_name"
const val COL_TODO_ID           = "todo_id"
const val COL_TAG_ID            = "tag_id"
