package com.egmdevelopers.sqlrelationsexample.sql.crud

import android.content.ContentValues
import android.util.Log
import com.egmdevelopers.sqlrelationsexample.sql.DatabaseHelper
import com.egmdevelopers.sqlrelationsexample.sql.constants.COL_CREATED_AT
import com.egmdevelopers.sqlrelationsexample.sql.constants.COL_ID
import com.egmdevelopers.sqlrelationsexample.sql.constants.COL_TAG_NAME
import com.egmdevelopers.sqlrelationsexample.sql.constants.TAG_TABLE_NAME
import com.egmdevelopers.sqlrelationsexample.sql.models.Tag
import java.util.*
import kotlin.collections.ArrayList

private val TAG = "CRUD_TAG"

/****************************************************************************************************/
/**          CREATE                                                                                 */
/****************************************************************************************************/
fun DatabaseHelper.createTag(tag: Tag): Long {
    val db = this.writableDatabase

    // Cargamos la información a la petición
    val values = ContentValues()
    values.apply {
        put(COL_TAG_NAME, tag.tagName)
        put(COL_CREATED_AT, Date().time)
    }

    // Insertamos el registro, nos devuelve el id asignado
    // Devolvemos el ID del registro creado en TAG
    return db.insert(TAG_TABLE_NAME, null, values)
}


/****************************************************************************************************/
/**          READ                                                                                   */
/****************************************************************************************************/
fun DatabaseHelper.getAllTags(): ArrayList<Tag> {
    val tags = ArrayList<Tag>()
    val select = "SELECT * FROM $TAG_TABLE_NAME"
    Log.i(TAG, select)

    val db = this.readableDatabase
    val c = db.rawQuery(select, null)

    if (c == null) {
        c?.close()
        return tags
    }

    if (c.moveToFirst()) {
        do {
            val tag     = Tag()
            tag.id      = c.getInt(c.getColumnIndex(COL_ID))
            tag.tagName = c.getString(c.getColumnIndex(COL_TAG_NAME))
            tags.add(tag)
        } while (c.moveToNext())
    }
    c.close()

    return tags
}

/****************************************************************************************************/
/**          UPDATE                                                                                 */
/****************************************************************************************************/
fun DatabaseHelper.updateTag(tag: Tag): Long {
    val db = this.writableDatabase
    val values = ContentValues()
    values.apply {
        put(COL_TAG_NAME, tag.tagName)
    }

    return db.update(TAG_TABLE_NAME, values,
        "$COL_ID = ?",
        arrayOf(tag.id.toString())).toLong()
}


/****************************************************************************************************/
/**          DELETE                                                                                 */
/****************************************************************************************************/
fun DatabaseHelper.deleteTag(tag: Tag, deleteTodosAssigned: Boolean) {

    // Verificamos si se desean eliminar los To_do's asignados
    if (deleteTodosAssigned) {
        val todos = getTodosWithTag(tag.tagName)
        for (todo in todos) {
            deleteTodo(todo.id)
        }
    } else {
        /* Actualizar el TAG del TO_DO asignado */
        val todos = getTodosWithTag(tag.tagName)
        // Eliminamos TodoTag
    }

    val db = this.writableDatabase
    db.delete(TAG_TABLE_NAME, "$COL_ID = ?",
        arrayOf(tag.id.toString()))
}
