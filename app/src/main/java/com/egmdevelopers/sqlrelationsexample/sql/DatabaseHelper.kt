package com.egmdevelopers.sqlrelationsexample.sql

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.egmdevelopers.sqlrelationsexample.sql.constants.*

class DatabaseHelper(context: Context,
                     factory: SQLiteDatabase.CursorFactory?)
    : SQLiteOpenHelper(context, DB_NAME, factory, DB_VERSION) {


    /************************************************************************************************/
    /**     CICLO DE VIDA                                                                           */
    /************************************************************************************************/
    override fun onCreate(db: SQLiteDatabase?) {
        // Creamos las tablas que componen la Base de Datos
        db?.apply {
            execSQL(CREATE_TABLE_TODO)
            execSQL(CREATE_TABLE_TAG)
            execSQL(CREATE_TABLE_TODO_TAG)
        }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        // En caso de requerir actualizar el esquema de la base de datos...
        db?.apply {
            execSQL(DROP_IF_EXISTS + TODO_TAG_TABLE_NAME)
            execSQL(DROP_IF_EXISTS + TODO_TABLE_NAME)
            execSQL(DROP_IF_EXISTS + TAG_TABLE_NAME)
        }

        // Creamos el nuevo esquema de la base de datos
        onCreate(db)
    }


    fun closeDB() {
        val db = this.readableDatabase
        if (db != null && db.isOpen) {
            db.close()
        }
    }





    /************************************************************************************************/
    /**     COMPANION OBJECT                                                                        */
    /************************************************************************************************/
    companion object {
        private val TAG = DatabaseHelper::class.java.simpleName
    }
}