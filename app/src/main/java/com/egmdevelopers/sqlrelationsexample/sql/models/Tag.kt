package com.egmdevelopers.sqlrelationsexample.sql.models

/*
 * Version 1 = "tags"
 * | NAME       | TYPE      | CONSTRAINTS   |
 * |------------|-----------|---------------|
 * | id         | INTEGER   | PK            |
 * | tag_name   | TEXT      | --            |
 * | created_at | DATETIME  | --            |
 */

data class Tag(var id       : Int,
               var tagName  : String) {

    constructor()               : this(0, "")
    constructor(tagName: String): this(0, tagName)
    // El constructor completo es la clase por sí misma

    // GETTERS y SETTERS definidos por default en las variables de la clase,
    // obteniendo su estatus de privacidad (al ser públicos, tendremos getters y setters
    // de cada una de las propiedades de la clase)

}