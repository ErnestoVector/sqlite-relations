package com.egmdevelopers.sqlrelationsexample.sql.crud

import android.content.ContentValues
import android.util.Log
import com.egmdevelopers.sqlrelationsexample.sql.DatabaseHelper
import com.egmdevelopers.sqlrelationsexample.sql.constants.*
import com.egmdevelopers.sqlrelationsexample.sql.models.Todo

/*
 * Version 1 = "todos"
 * | NAME       | TYPE      | CONSTRAINTS   |
 * |------------|-----------|---------------|
 * | id         | INTEGER   | PK            |
 * | note       | TEXT      | --            |
 * | created_at | DATETIME  | --            |
 */


private const val TAG = "CRUD_TODO"

/****************************************************************************************************/
/**          CREATE                                                                                 */
/****************************************************************************************************/
fun DatabaseHelper.createTodo(todo: Todo/*, tagIds: List<Long>*/): Long {
    val db = this.writableDatabase

    // Cargamos la información a la petición
    val values = ContentValues()
    values.apply {
        put(COL_TODO, todo.note)
        put(COL_STATUS, todo.status)
        put(COL_CREATED_AT, todo.createdAt)
    }

    // Insertamos el registro, nos devuelve el id asignado
    val todoId = db.insert(TODO_TABLE_NAME, null, values)

    // Asignamos las etiquetas a To_Do
    //for (tag in tagIds) {
    //    createTodoTag(todoId, tag)
    //}

    // Devolvemos el ID del registro creado en TO_DO
    return todoId
}


/****************************************************************************************************/
/**          READ                                                                                   */
/****************************************************************************************************/

/**
 * Lee un registro específico obtenido por el ID
 *
 * @param todoId Id que queremos obtener
 * @return Objeto To_do con los datos leídos
 */
fun DatabaseHelper.getTodo(todoId: Long): Todo? {
    val db = this.readableDatabase

    // Realizamos el SELECT
    val select = "SELECT * FROM $TODO_TABLE_NAME WHERE $COL_ID = $todoId"
    Log.i(TAG, select)

    val c = db.rawQuery(select, null)

    if (c == null) {
        c?.close()
        return null
    }

    // Parseamos la información recibida
    c.moveToFirst()
    val todo = Todo()
    todo.apply {
        id          = c.getInt(c.getColumnIndex(COL_ID))
        note        = c.getString(c.getColumnIndex(COL_TODO))
        createdAt   = c.getString(c.getColumnIndex(COL_CREATED_AT))
    }
    c.close()


    // Devolvemos el elemento seleccionado
    return todo
}

/**
 * Lee todos los registros almacenados en la tabla
 *
 * @return Lista de To_Do
 */
fun DatabaseHelper.getAllTodos(): List<Todo> {
    val data = ArrayList<Todo>()
    val select = "SELECT * FROM $TODO_TABLE_NAME"

    Log.i(TAG, select)

    val db = this.readableDatabase
    val c = db.rawQuery(select, null)

    if (c == null) {
        c?.close()
        return data
    }

    if (c.moveToFirst()) {
        do {
            val result = Todo()
            result.apply {
                id          = c.getInt(c.getColumnIndex(COL_ID))
                note        = c.getString(c.getColumnIndex(COL_TODO))
                createdAt   = c.getString(c.getColumnIndex(COL_CREATED_AT))

                data.add(result)
            }
        } while (c.moveToNext())
    }
    c.close()

    return data
}

/**
 * Lee todos los registros por TAG
 *
 * @return Lista de To_Do con el TAG elegido
 */
fun DatabaseHelper.getTodosWithTag(tagName: String): List<Todo> {
    val data = ArrayList<Todo>()
    val select = (
            "SELECT  * FROM " +
                    TODO_TABLE_NAME + " td, " +
                    TAG_TABLE_NAME + " tg, " +
                    TODO_TAG_TABLE_NAME + " tt " +
                    " WHERE tg." + COL_TAG_NAME + " = '" + tagName + "'" +
                    " AND tg." + COL_ID + " = " + "tt." + COL_TAG_ID +
                    " AND td." + COL_ID + " = " + "tt." + COL_TODO_ID
            )
    Log.i(TAG, select)

    val db = this.readableDatabase
    val c = db.rawQuery(select, null)

    if (c == null) {
        c?.close()
        return data
    }

    if (c.moveToFirst()) {
        do {
            val result = Todo()
            result.apply {
                id          = c.getInt(c.getColumnIndex(COL_ID))
                note        = c.getString(c.getColumnIndex(COL_TODO))
                createdAt   = c.getString(c.getColumnIndex(COL_CREATED_AT))

                data.add(result)
            }
        } while (c.moveToNext())
    }
    c.close()

    return data
}

/****************************************************************************************************/
/**          UPDATE                                                                                 */
/****************************************************************************************************/
fun DatabaseHelper.updateTodo(todo: Todo): Long {
    val db = this.writableDatabase
    val values = ContentValues()
    values.apply {
        put(COL_TODO, todo.note)
        put(COL_STATUS, todo.status)
    }

    return db.update(TODO_TABLE_NAME, values,
        "$COL_ID = ?", arrayOf(todo.id.toString())).toLong()
}


fun DatabaseHelper.updateNoteTag(id: Long, tagId: Long): Int {
    val db = this.writableDatabase

    val values = ContentValues()
    values.put(COL_TAG_ID, tagId)

    // Actualizando el renglón
    return db.update(TODO_TABLE_NAME, values, "$COL_ID = ?", arrayOf(id.toString()))
}

/****************************************************************************************************/
/**          DELETE                                                                                 */
/****************************************************************************************************/
fun DatabaseHelper.deleteTodo(todoId: Int) {
    val db = this.writableDatabase
    db.delete(TODO_TABLE_NAME, "$COL_ID = ?", arrayOf(todoId.toString()))
}