package com.egmdevelopers.sqlrelationsexample.sql.constants

// Constantes para QWERY's
const val DROP_IF_EXISTS    = "DROP TABLE IF EXISTS "

// Creación de la tabla TO_DO versión 1
const val CREATE_TABLE_TODO = (
        "CREATE TABLE " + TODO_TABLE_NAME + "(" +
                COL_ID + " INTEGER PRIMARY KEY," +
                COL_TODO + " TEXT," +
                COL_STATUS + " INTEGER," +
                COL_CREATED_AT + " DATETIME" + ")"
        )

// Creación de la tabla TAG versión 1
const val CREATE_TABLE_TAG = (
        "CREATE TABLE " + TAG_TABLE_NAME + "(" +
                COL_ID + " INTEGER PRIMARY KEY," +
                COL_TAG_NAME + " TEXT," +
                COL_CREATED_AT + " DATETIME" + ")"
        )

const val CREATE_TABLE_TODO_TAG = (
        "CREATE TABLE " + TODO_TAG_TABLE_NAME + "(" +
                COL_ID + " INTEGER PRIMARY KEY," +
                COL_TODO_ID + " INTEGER," +
                COL_TAG_ID + " INTEGER" + ")"
        )