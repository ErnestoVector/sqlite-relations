package com.egmdevelopers.sqlrelationsexample.sql.models

/*
 * Version 1 = "todos"
 * | NAME       | TYPE      | CONSTRAINTS   |
 * |------------|-----------|---------------|
 * | id         | INTEGER   | PK            |
 * | note       | TEXT      | --            |
 * | created_at | DATETIME  | --            |
 */
data class Todo(var id          : Int,
                var note        : String,
                var status      : Int,
                var createdAt   : String) {

    constructor()                                   : this(0, "", 0, "")
    constructor(note: String, status: Int)          : this(0, note, status, "")
    constructor(id: Int, note: String, status: Int) : this(id, note, status, "")
    // El constructor completo es la clase por sí misma

    // GETTERS y SETTERS definidos por default en las variables de la clase,
    // obteniendo su estatus de privacidad (al ser públicos, tendremos getters y setters
    // de cada una de las propiedades de la clase)

}