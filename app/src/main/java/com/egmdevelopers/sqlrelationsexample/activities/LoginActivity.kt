package com.egmdevelopers.sqlrelationsexample.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.egmdevelopers.sqlrelationsexample.R
import com.egmdevelopers.sqlrelationsexample.extensions.toast

import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.content_login.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    /************************************************************************************************/
    /**     CICLO DE VIDA                                                                           */
    /************************************************************************************************/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setOnClickListeners()
    }

    private fun setOnClickListeners() {
        fab.setOnClickListener(this)
    }


    /************************************************************************************************/
    /**     ON CLICK                                                                                */
    /************************************************************************************************/
    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.fab -> checkLogin()
        }
    }


    private fun checkLogin() {
        val email = etEmail.text.toString()
        val password = etPassword.text.toString()

        when {
            email.isBlank()         -> toast("Ingresa tu correo")
            password.isBlank()      -> toast("Ingresa tu contraseña")
            email != CORRECT_EMAIL  -> toast("Usuario no registrado")
            password != CORRECT_PASS-> toast("Contraseña incorrecta")
            else -> {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }




    companion object {
        private const val CORRECT_EMAIL = "ernesto@ejemplo.com"
        private const val CORRECT_PASS  = "12345678"
    }

}
