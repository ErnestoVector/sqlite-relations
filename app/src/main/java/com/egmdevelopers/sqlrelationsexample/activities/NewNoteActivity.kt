package com.egmdevelopers.sqlrelationsexample.activities

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.egmdevelopers.sqlrelationsexample.R
import com.egmdevelopers.sqlrelationsexample.extensions.toast
import com.egmdevelopers.sqlrelationsexample.sql.DatabaseHelper
import com.egmdevelopers.sqlrelationsexample.sql.crud.createTodo
import com.egmdevelopers.sqlrelationsexample.sql.models.Todo

import kotlinx.android.synthetic.main.activity_new_note.*
import kotlinx.android.synthetic.main.content_new_note.*

class NewNoteActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var db : DatabaseHelper

    /************************************************************************************************/
    /**     CICLO DE VIDA                                                                           */
    /************************************************************************************************/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_note)
        setOnClickListeners()
        db = DatabaseHelper(applicationContext, null)
    }

    override fun onDestroy() {
        super.onDestroy()
        db.closeDB()
    }


    private fun setOnClickListeners() {
        fabSet.setOnClickListener(this)
    }




    /************************************************************************************************/
    /**     ON CLICK                                                                                */
    /************************************************************************************************/
    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.fabSet -> storeInDatabase()
        }
    }


    private fun storeInDatabase() {
        val note = etNote.text.toString()

        if (note.isBlank()) {
            toast("Ingresa una nota...")
            return
        }

        val data = Todo(note, 0)
        val id = db.createTodo(data)

        Log.d(TAG, "Elemento agregado: $id")
        toast("Se agregó correctamente la nota")

        // Limpiamos la nota recién guardada
        etNote.setText("")
    }



    /************************************************************************************************/
    /**     COMPANION OBJECT                                                                        */
    /************************************************************************************************/
    companion object {
        private val TAG = NewNoteActivity::class.java.simpleName
    }
}
