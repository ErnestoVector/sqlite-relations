package com.egmdevelopers.sqlrelationsexample.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.ContextMenu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListAdapter
import android.widget.PopupMenu
import com.egmdevelopers.sqlrelationsexample.R
import com.egmdevelopers.sqlrelationsexample.extensions.toast
import com.egmdevelopers.sqlrelationsexample.sql.DatabaseHelper
import com.egmdevelopers.sqlrelationsexample.sql.crud.deleteTodo
import com.egmdevelopers.sqlrelationsexample.sql.crud.getAllTodos
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity(),
    View.OnClickListener,
    AdapterView.OnItemLongClickListener {

    private lateinit var db: DatabaseHelper
    private val titles = ArrayList<String>()

    /************************************************************************************************/
    /**     CICLO DE VIDA                                                                           */
    /************************************************************************************************/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setOnClickListeners()
        db = DatabaseHelper(applicationContext, null)
        initListView()
    }

    override fun onResume() {
        super.onResume()
        registerForContextMenu(lvNotes)
    }

    override fun onDestroy() {
        super.onDestroy()
        db.closeDB()
    }


    private fun setOnClickListeners() {
        fabAdd.setOnClickListener(this)
        lvNotes.onItemLongClickListener = this
    }

    private fun initListView() {
        val notes = db.getAllTodos()
        titles.clear()
        for (note in notes) {
            titles.add(note.note)
        }
        lvNotes.adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, titles)
    }

    /************************************************************************************************/
    /**     ON CLICK                                                                                */
    /************************************************************************************************/
    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.fabAdd -> newNote()
        }
    }

    override fun onItemLongClick(parent: AdapterView<*>?,
                                 view: View?,
                                 position: Int,
                                 id: Long): Boolean {
        val notes = db.getAllTodos()
        for (note in notes) {
            if (note.note == titles[position]) {
                db.deleteTodo(note.id)
            }
        }
        toast("Eliminando tarea")
        initListView()
        return true
    }

    private fun newNote() {
        val intent = Intent(this, NewNoteActivity::class.java)
        startActivity(intent)
    }

    /************************************************************************************************/
    /**     COMPANION OBJECT                                                                        */
    /************************************************************************************************/
    companion object {
        private val TAG = MainActivity::class.java.simpleName
    }

}
